<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nghiablog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NDo7G+zV!st):B/>9*WUf@11?AhLSs0q>r~1~B4[Ss_Of;mx8IjL:[51YGJi3H=p');
define('SECURE_AUTH_KEY',  'P$r ru`>fX4?f>Pv.wGU4na7V@:1&FrgI9|o.?B? .<FYesPbqNU&-N*}(0CC(87');
define('LOGGED_IN_KEY',    'Lb~EyuN67Zs8abq7=q`ua1nE|T;RVo[wO4lMlqUygg^ pNi.|Rmpt _Z>;g:3cWo');
define('NONCE_KEY',        '2<kBp5SD^ZUiO+s<Sn[qcAkcW>bGB|cwBM}=@1~k[d_1ejKOa.uqE_&pUM.HB84P');
define('AUTH_SALT',        ']emd.}{]mvvc*FMhzis.<K,aTZ*C,(|K<)xH.!To?ID0H1n-7M!SnW=X9:hiH{`g');
define('SECURE_AUTH_SALT', '5U ]>WNTJq/WBH;&.P{rh|P_[h#r;6+nC013M;iFx.EescO|/%@^JU4+yO#Zn3Y$');
define('LOGGED_IN_SALT',   '+KK@xZJr.)_QG= J? gf~8C-OFm Q<NE7,v!mj@]3 MS!&uQr*ai,6g*x_Nu 9K=');
define('NONCE_SALT',       'hq,79fr07DlPq#4Ykn2NLa6Y`34_|?-=1!>1u-w+Zb&h*+NRrjq$, ;]!G:%/qPD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
